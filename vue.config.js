module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? '/google-app/'
        : '/',
    pwa: {
        manifestOptions: {
            display: 'fullscreen'
        }
    }
};
